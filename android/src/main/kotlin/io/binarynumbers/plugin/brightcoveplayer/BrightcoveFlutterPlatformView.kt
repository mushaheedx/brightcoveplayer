package io.binarynumbers.plugin.brightcoveplayer

import android.content.Context
import android.view.View
import android.widget.RelativeLayout
import com.brightcove.player.edge.Catalog
import com.brightcove.player.edge.VideoListener
import com.brightcove.player.model.DeliveryType
import com.brightcove.player.model.Video
import com.brightcove.player.view.BrightcoveExoPlayerVideoView
import io.flutter.plugin.platform.PlatformView


internal class BrightcoveFlutterPlatformView(context: Context, id: Int, creationParams: Map<String?, Any?>?) : PlatformView {
    private var brightcoveVideoView: BrightcoveExoPlayerVideoView

    override fun getView(): View {
        return brightcoveVideoView
    }

    override fun dispose() {}

    init {
        // get parameter
        val accountId: String = creationParams?.get("account_id") as String? ?: ""
        val policyId: String = creationParams?.get("policy_id") as String? ?: ""
        val videoId: String = creationParams?.get("video_id") as String? ?: ""

        println("Using account: $accountId, with policy:$policyId")
        println("Opening video: $videoId")

        // define UI & initialize
        brightcoveVideoView = BrightcoveExoPlayerVideoView(context)
        brightcoveVideoView.layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
        brightcoveVideoView.finishInitialization()

        val analytics = brightcoveVideoView.analytics
        analytics.account = accountId

         val eventEmitter = brightcoveVideoView.eventEmitter

        val catalog = Catalog(eventEmitter, accountId, policyId)

        catalog.findVideoByID(videoId, object : VideoListener() {
            override fun onVideo(video: Video?) {
                if (video != null) {
                    brightcoveVideoView.add(video)
                    brightcoveVideoView.start()
                }
            }
        })

//        brightcoveVideoView.add(video);

        brightcoveVideoView.start();
    }

}