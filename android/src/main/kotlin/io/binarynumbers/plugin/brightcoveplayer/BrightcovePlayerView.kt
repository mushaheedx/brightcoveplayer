//package io.binarynumbers.plugin.brightcoveplayer
//
//import android.content.Context
//import android.graphics.Color
//import android.util.Log
//import android.view.SurfaceView
//import android.widget.RelativeLayout
//import androidx.core.view.ViewCompat
//import com.brightcove.player.display.ExoPlayerVideoDisplayComponent
//import com.brightcove.player.edge.Catalog
//import com.brightcove.player.edge.OfflineCatalog
//import com.brightcove.player.edge.VideoListener
//import com.brightcove.player.event.Event
//import com.brightcove.player.event.EventType
//import com.brightcove.player.mediacontroller.BrightcoveMediaController
//import com.brightcove.player.model.Video
//import com.brightcove.player.view.BrightcoveExoPlayerVideoView
//import com.google.android.exoplayer2.C
//import com.google.android.exoplayer2.PlaybackParameters
//import com.google.android.exoplayer2.RendererCapabilities
//import com.google.android.exoplayer2.trackselection.DefaultTrackSelector.SelectionOverride
//import com.google.android.exoplayer2.trackselection.FixedTrackSelection
//import com.google.android.exoplayer2.trackselection.TrackSelection
//import java.util.*
//
//data class BrightcoveAccountInfo(val policyKey: String, val accountId: String)
//data class BrightcoveVideoInfo(val videoId: String, val referenceId: String, val videoToken: String, val catalog: Catalog, val offlineCatalog: OfflineCatalog)
//
//class BrightcovePlayerView(context: Context, accountInfo: BrightcoveAccountInfo, autoPlay: Boolean = true, videoInfo: BrightcoveVideoInfo) : RelativeLayout(context) {
//    private val playerVideoView: BrightcoveExoPlayerVideoView
//    private val mediaController: BrightcoveMediaController
//    private var playing: Boolean = false
//    private var bitRate = 0
//    private var playbackRate = 1f
//
//    private lateinit var videoInfo: BrightcoveVideoInfo
//
//    fun changeVideo(videoInfo: BrightcoveVideoInfo) {
//        this.videoInfo = videoInfo
//    }
//
//    fun setPlay(play: Boolean) {
//        if (playing == play) return
//        if (play) {
//            playerVideoView.start()
//        } else {
//            playerVideoView.pause()
//        }
//    }
//
//    fun setDefaultControlDisabled(disabled: Boolean) {
//        mediaController.hide()
//        mediaController.setShowHideTimeout(if (disabled) 1 else 4000)
//    }
//
//    fun setFullscreen(fullscreen: Boolean) {
//        mediaController.show()
//        val event: WritableMap = Arguments.createMap()
//        event.putBoolean("fullscreen", fullscreen)
//        val reactContext: ReactContext = getContext() as ReactContext
//        reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(this@BrightcovePlayerView.id, BrightcovePlayerManager.EVENT_TOGGLE_ANDROID_FULLSCREEN, event)
//    }
//
//    fun setVolume(volume: Float) {
//        val details: MutableMap<String, Any> = HashMap()
//        details[Event.VOLUME] = volume
//        playerVideoView.eventEmitter.emit(EventType.SET_VOLUME, details)
//    }
//
//    fun setBitRate(bitRate: Int) {
//        this.bitRate = bitRate
//        updateBitRate()
//    }
//
//    fun setPlaybackRate(playbackRate: Float) {
//        if (playbackRate == 0f) return
//        this.playbackRate = playbackRate
//        updatePlaybackRate()
//    }
//
//    fun seekTo(time: Int) {
//        playerVideoView.seekTo(time)
//    }
//
//    private fun updateBitRate() {
//        if (bitRate == 0) return
//        val videoDisplay = playerVideoView.videoDisplay as ExoPlayerVideoDisplayComponent
//        val player = videoDisplay.exoPlayer
//        val trackSelector = videoDisplay.trackSelector
//        if (player == null) return
//        val mappedTrackInfo = trackSelector!!.currentMappedTrackInfo ?: return
//        var rendererIndex: Int? = null
//        for (i in 0 until mappedTrackInfo.length) {
//            val trackGroups = mappedTrackInfo.getTrackGroups(i)
//            if (trackGroups.length != 0 && player.getRendererType(i) == C.TRACK_TYPE_VIDEO) {
//                rendererIndex = i
//                break
//            }
//        }
//        if (rendererIndex == null) return
//        if (bitRate == 0) {
//            trackSelector.clearSelectionOverrides(rendererIndex)
//            return
//        }
//        var resultBitRate = -1
//        var targetGroupIndex = -1
//        var targetTrackIndex = -1
//        val trackGroups = mappedTrackInfo.getTrackGroups(rendererIndex)
//        for (groupIndex in 0 until trackGroups.length) {
//            val group = trackGroups[groupIndex]
//            if (group != null) {
//                for (trackIndex in 0 until group.length) {
//                    val format = group.getFormat(trackIndex)
//                    if (format != null && mappedTrackInfo.getTrackFormatSupport(rendererIndex, groupIndex, trackIndex)
//                            == RendererCapabilities.FORMAT_HANDLED) {
//                        if (resultBitRate == -1 ||
//                                (if (resultBitRate > bitRate) format.bitrate < resultBitRate else format.bitrate <= bitRate && format.bitrate > resultBitRate)) {
//                            targetGroupIndex = groupIndex
//                            targetTrackIndex = trackIndex
//                            resultBitRate = format.bitrate
//                        }
//                    }
//                }
//            }
//        }
//        if (targetGroupIndex != -1 && targetTrackIndex != -1) {
//            trackSelector.setSelectionOverride(rendererIndex, trackGroups,
//                    SelectionOverride(targetGroupIndex, targetTrackIndex))
//        }
//    }
//
//    private fun updatePlaybackRate() {
//        val expPlayer = (playerVideoView.videoDisplay as ExoPlayerVideoDisplayComponent).exoPlayer
//        expPlayer?.setPlaybackParameters(PlaybackParameters(playbackRate, 1f))
//    }
//
//
//
//    private fun loadVideo() {
//        if (videoToken != null && videoToken != "") {
//            offlineCatalog = OfflineCatalog(context, playerVideoView.eventEmitter, accountId, policyKey)
//            try {
//                val video = offlineCatalog!!.findOfflineVideoById(videoToken)
//                video?.let { playVideo(it) }
//            } catch (e: Exception) {
//            }
//            return
//        }
//        val listener: VideoListener = object : VideoListener() {
//            override fun onVideo(video: Video) {
//                playVideo(video)
//            }
//        }
//        catalog = Catalog(playerVideoView.eventEmitter, accountId, policyKey)
//        if (videoId != null) {
//            catalog!!.findVideoByID(videoId!!, listener)
//        } else if (referenceId != null) {
//            catalog!!.findVideoByReferenceID(referenceId!!, listener)
//        }
//    }
//
//    private fun playVideo(video: Video) {
//        playerVideoView.clear()
//        playerVideoView.add(video)
//        if (autoPlay) {
//            playerVideoView.start()
//        }
//    }
//
//    private fun fixVideoLayout() {
//        val viewWidth = this.measuredWidth
//        val viewHeight = this.measuredHeight
//        val surfaceView = playerVideoView.renderView as SurfaceView?
//        surfaceView!!.measure(viewWidth, viewHeight)
//        val surfaceWidth = surfaceView.measuredWidth
//        val surfaceHeight = surfaceView.measuredHeight
//        val leftOffset = (viewWidth - surfaceWidth) / 2
//        val topOffset = (viewHeight - surfaceHeight) / 2
//        surfaceView.layout(leftOffset, topOffset, leftOffset + surfaceWidth, topOffset + surfaceHeight)
//    }
//
//    private fun printKeys(map: Map<String, Any>) {
//        Log.d("debug", "-----------")
//        for ((key) in map) {
//            Log.d("debug", key)
//        }
//    }
//
//    fun onHostResume() {}
//    fun onHostPause() {}
//    fun onHostDestroy() {
//        playerVideoView.destroyDrawingCache()
//        playerVideoView.clear()
//        removeAllViews()
//    }
//
//    companion object {
//        private val FIXED_FACTORY: TrackSelection.Factory = FixedTrackSelection.Factory()
//    }
//
//    init {
//        this.videoInfo = videoInfo
//        setBackgroundColor(Color.BLACK)
//        playerVideoView = BrightcoveExoPlayerVideoView(this.context)
//        this.addView(playerVideoView)
//        playerVideoView.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
//        playerVideoView.finishInitialization()
//        mediaController = BrightcoveMediaController(playerVideoView)
//        playerVideoView.setMediaController(mediaController)
//        requestLayout()
//        ViewCompat.setTranslationZ(this, 9999f)
//        val eventEmitter = playerVideoView.eventEmitter
//        eventEmitter.on(EventType.VIDEO_SIZE_KNOWN) {
//            fixVideoLayout()
//            updateBitRate()
//            updatePlaybackRate()
//        }
//        eventEmitter.on(EventType.READY_TO_PLAY) {
//            val event: WritableMap = Arguments.createMap()
//            val reactContext: ReactContext = getContext() as ReactContext
//            reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(this@BrightcovePlayerView.id, BrightcovePlayerManager.EVENT_READY, event)
//        }
//        eventEmitter.on(EventType.DID_PLAY) {
//            playing = true
//            val event: WritableMap = Arguments.createMap()
//            val reactContext: ReactContext = getContext() as ReactContext
//            reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(this@BrightcovePlayerView.id, BrightcovePlayerManager.EVENT_PLAY, event)
//        }
//        eventEmitter.on(EventType.DID_PAUSE) {
//            playing = false
//            val event: WritableMap = Arguments.createMap()
//            val reactContext: ReactContext = getContext() as ReactContext
//            reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(this@BrightcovePlayerView.id, BrightcovePlayerManager.EVENT_PAUSE, event)
//        }
//        eventEmitter.on(EventType.COMPLETED) {
//            val event: WritableMap = Arguments.createMap()
//            val reactContext: ReactContext = getContext() as ReactContext
//            reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(this@BrightcovePlayerView.id, BrightcovePlayerManager.EVENT_END, event)
//        }
//        eventEmitter.on(EventType.PROGRESS) { e ->
//            val event: WritableMap = Arguments.createMap()
//            val playhead = e.properties[Event.PLAYHEAD_POSITION] as Int?
//            event.putDouble("currentTime", playhead!! / 1000.0)
//            val reactContext: ReactContext = getContext() as ReactContext
//            reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(this@BrightcovePlayerView.id, BrightcovePlayerManager.EVENT_PROGRESS, event)
//        }
//        eventEmitter.on(EventType.ENTER_FULL_SCREEN) {
//            mediaController.show()
//            val event: WritableMap = Arguments.createMap()
//            val reactContext: ReactContext = getContext() as ReactContext
//            reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(this@BrightcovePlayerView.id, BrightcovePlayerManager.EVENT_TOGGLE_ANDROID_FULLSCREEN, event)
//        }
//        eventEmitter.on(EventType.EXIT_FULL_SCREEN) {
//            mediaController.show()
//            val event: WritableMap = Arguments.createMap()
//            val reactContext: ReactContext = getContext() as ReactContext
//            reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(this@BrightcovePlayerView.id, BrightcovePlayerManager.EVENT_TOGGLE_ANDROID_FULLSCREEN, event)
//        }
//        eventEmitter.on(EventType.VIDEO_DURATION_CHANGED) { e ->
//            val duration = e.properties[Event.VIDEO_DURATION] as Int?
//            val event: WritableMap = Arguments.createMap()
//            event.putDouble("duration", duration!! / 1000.0)
//            val reactContext: ReactContext = getContext() as ReactContext
//            reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(this@BrightcovePlayerView.id, BrightcovePlayerManager.EVENT_CHANGE_DURATION, event)
//        }
//        eventEmitter.on(EventType.BUFFERED_UPDATE) { e ->
//            val percentComplete = e.properties[Event.PERCENT_COMPLETE] as Int?
//            val event: WritableMap = Arguments.createMap()
//            event.putDouble("bufferProgress", percentComplete!! / 100.0)
//            val reactContext: ReactContext = getContext() as ReactContext
//            reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(this@BrightcovePlayerView.id, BrightcovePlayerManager.EVENT_UPDATE_BUFFER_PROGRESS, event)
//        }
//    }
//}