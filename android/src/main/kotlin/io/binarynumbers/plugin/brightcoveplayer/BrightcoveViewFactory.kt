package io.binarynumbers.plugin.brightcoveplayer

import android.content.Context

import io.flutter.plugin.common.StandardMessageCodec
import io.flutter.plugin.platform.PlatformView
import io.flutter.plugin.platform.PlatformViewFactory

internal class BrightcoveViewFactory() : PlatformViewFactory(StandardMessageCodec.INSTANCE) {
    override fun create(context: Context, id: Int, args: Any?): PlatformView {
        val creationParams:  Map<String?, Any?>? = args as  Map<String?, Any?>?
        return BrightcoveFlutterPlatformView(context, id, creationParams)
    }
}