#import "BrightcoveplayerPlugin.h"
#if __has_include(<brightcoveplayer/brightcoveplayer-Swift.h>)
#import <brightcoveplayer/brightcoveplayer-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "brightcoveplayer-Swift.h"
#endif

@implementation BrightcoveplayerPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftBrightcoveplayerPlugin registerWithRegistrar:registrar];
}
@end
