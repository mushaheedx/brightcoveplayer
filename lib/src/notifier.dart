import 'dart:async';

import 'package:flutter/foundation.dart';

class BrightcoveNotifier with ChangeNotifier {
  static const _visibilityDuration = const Duration(milliseconds: 3000);

  Timer _visibilityTimer;

  void _cancelVisibilityTimer() {
    if (_visibilityTimer == null) return;

    _visibilityTimer.cancel();
    _visibilityTimer = null;
  }

  void _onButtonVisible() async {
    // cancel previous timer
    _cancelVisibilityTimer();
    // start new timer
    _visibilityTimer = Timer(
      _visibilityDuration,
      _hideButton,
    );
  }

  bool _isBackButtonVisible = false;

  bool get isBackButtonVisible => _isBackButtonVisible ?? false;

  void toggleBackButtonVisibility() {
    print('[BrightCoveNotifier] Toggling back button visibility');
    _isBackButtonVisible = !isBackButtonVisible;
    notifyListeners();
    if (_isBackButtonVisible) _onButtonVisible();
    if (!_isBackButtonVisible) _cancelVisibilityTimer();
  }

  void _hideButton() {
    _cancelVisibilityTimer();
    if (!_isBackButtonVisible) return;
    _isBackButtonVisible = false;
    notifyListeners();
  }

  @override
  void dispose() {
    _cancelVisibilityTimer();
    super.dispose();
  }
}
