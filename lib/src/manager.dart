import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import 'notifier.dart';

typedef Future<void> BrightcoveCloseCallback(BuildContext context);

class BrightcoveManager {
  final String accountId;
  final String policyId;
  final String videoId;

  static const _name = '[BrightcoveManager]';

  /// push a route
  final BrightcoveCloseCallback onCloseCallback;

  BrightcoveManager._(
    this.accountId,
    this.policyId,
    this.videoId,
    this.onCloseCallback,
  ) : notifier = BrightcoveNotifier();

  Map<String, dynamic> get properties => {
        'account_id': accountId,
        'policy_id': policyId,
        'video_id': videoId,
      };

  /// App must push-replace a route in [onCloseCallback].
  /// Popping this route (which happens by default) may not restore UI changes.
  static Future<BrightcoveManager> initialize({
    @required String accountId,
    @required String policyId,
    @required String videoId,
    BrightcoveCloseCallback onCloseCallback,
  }) async {
    assert(accountId.isNotEmpty && policyId.isNotEmpty && videoId.isNotEmpty);

    final _instance = BrightcoveManager._(
      accountId,
      policyId,
      videoId,
      onCloseCallback,
    );

    await _instance.adjustUI();
    return _instance;
  }

  BrightcoveNotifier notifier;

  bool _mustRestore = false;

  Future<void> adjustUI() async {
    _mustRestore = true;
    debugPrint('$_name Making overlay and orientation changes in UI');
    await SystemChrome.setEnabledSystemUIOverlays([]);
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
  }

  Future<void> restoreUI() async {
    if (!_mustRestore) return;
    _mustRestore = false;
    debugPrint('$_name Restoring UI');
    notifier.dispose();
    await SystemChrome.restoreSystemUIOverlays();
    await SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  Future<void> closePlayer(
    BuildContext context, {
    BrightcoveCloseCallback onClose,
  }) async {
    debugPrint('$_name Closing player');
    await restoreUI();

    if (onClose != null) {
      onClose(context);
      return;
    }

    if (onCloseCallback != null) {
      onCloseCallback(context);
      return;
    }

    Navigator.pop(context);
  }

  @Deprecated('USE [BrightcoveManager.closePlayer]')
  Future<bool> onPop(BuildContext context) async {
    await restoreUI();

    if (onCloseCallback == null) return true;

    onCloseCallback(context);

    return false;
  }

  void dispose() {
    restoreUI();
  }
}
