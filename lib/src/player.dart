import 'package:brightcoveplayer/src/players/android.dart';

import 'package:flutter/material.dart';

import 'gestures/gestures.dart';
import 'manager.dart';
import 'widgets/back_button.dart';

class BrightcovePlayer extends StatelessWidget {
  final BrightcoveManager manager;

  const BrightcovePlayer({
    Key key,
    @required this.manager,
  })  : assert(manager != null),
        super(key: key);

  Widget build(BuildContext context) {
    return TranslucentGestureDetector(
      onTap: manager.notifier.toggleBackButtonVisibility,
      child: Container(
        color: const Color(0xFF000000),
        constraints: const BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            AndroidBrightcovePlayer(
              manager: manager,
            ),
            Align(
              alignment: Alignment.topLeft,
              child: PlayerBackButton(
                manager: manager,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
