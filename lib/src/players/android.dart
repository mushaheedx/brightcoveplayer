import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import '../gestures/gesture_recognizer.dart';
import '../manager.dart';

class AndroidBrightcovePlayer extends StatelessWidget {
  final BrightcoveManager manager;

  const AndroidBrightcovePlayer({
    Key key,
    @required this.manager,
  })  : assert(manager != null),
        super(key: key);

  static const String viewType = 'NativeBrightcovePlayer';

  PlatformViewController onPlatformViewCreated(
    PlatformViewCreationParams params,
  ) {
    if (!Platform.isAndroid) {
      throw UnimplementedError('Only supported for android');
    }

    final controller = PlatformViewsService.initSurfaceAndroidView(
      id: params.id,
      viewType: viewType,
      layoutDirection: TextDirection.ltr,
      creationParams: manager.properties ?? <String, dynamic>{},
      creationParamsCodec: StandardMessageCodec(),
    );
    controller.addOnPlatformViewCreatedListener(params.onPlatformViewCreated);
    controller.create();
    return controller;
  }

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      throw UnimplementedError('Only supported for android');
    }
    final child = PlatformViewLink(
      surfaceFactory: (
        BuildContext context,
        PlatformViewController platformViewController,
      ) {
        return AndroidViewSurface(
          controller: platformViewController,
          gestureRecognizers: {
            Factory(() => BrightCoveFaultyMultiTapGestureRecognizer())
          },
          hitTestBehavior: PlatformViewHitTestBehavior.opaque,
        );
      },
      onCreatePlatformView: onPlatformViewCreated,
      viewType: viewType,
    );

    return WillPopScope(
      // ignore: deprecated_member_use_from_same_package
      onWillPop: () => manager.onPop(context),
      child: child,
    );
  }
}
