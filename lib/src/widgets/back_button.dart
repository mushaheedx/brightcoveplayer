import 'package:flutter/material.dart';

import '../../brightcoveplayer.dart';

/// Showing the media controls.  They will be hidden in 3000 milliseconds using animation style: FADE.
class PlayerBackButton extends StatefulWidget {
  final BrightcoveManager manager;

  const PlayerBackButton({Key key, @required this.manager}) : super(key: key);

  @override
  _PlayerBackButtonState createState() => _PlayerBackButtonState();
}

class _PlayerBackButtonState extends State<PlayerBackButton> {
  bool _isButtonVisible;

  @override
  void initState() {
    super.initState();
    _isButtonVisible = widget.manager.notifier.isBackButtonVisible;
    widget.manager.notifier.addListener(_valueChanged);
  }

  @override
  void dispose() {
    widget.manager.notifier.removeListener(_valueChanged);
    super.dispose();
  }

  void _valueChanged() {
    if (_isButtonVisible == widget.manager.notifier.isBackButtonVisible) return;
    setState(() {
      _isButtonVisible = widget.manager.notifier.isBackButtonVisible;
    });
  }

  static const _opacityChangeDuration = const Duration(
    milliseconds: 300,
  );

  @override
  Widget build(BuildContext context) {
    final double _opacity = _isButtonVisible ? 1 : 0;
    final Curve _curve = _isButtonVisible ? Curves.easeIn : Curves.easeOut;

    return Material(
      color: Colors.transparent,
      borderRadius: BorderRadius.circular(35),
      child: AnimatedOpacity(
        opacity: _opacity,
        duration: _opacityChangeDuration,
        curve: _curve,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 12,
            vertical: 12,
          ),
          child: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
            ),
            color: Colors.white,
            onPressed: () {
              widget.manager.closePlayer(context);
            },
          ),
        ),
      ),
    );
  }
}
