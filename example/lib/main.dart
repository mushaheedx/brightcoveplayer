import 'package:brightcoveplayer/brightcoveplayer.dart';
import 'package:flutter/material.dart';

const String title = 'Brightcove player demo';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: title,
      theme: ThemeData(
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Form(
        child: MyHomePage(
          title: title,
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController accountId;
  TextEditingController policyId;
  TextEditingController videoId;
  @override
  void initState() {
    super.initState();
    accountId = TextEditingController(
      text: '6219356502001',
    );
    policyId = TextEditingController(
      text:
          'BCpkADawqM3xGPAlcC7ZVHZ66gX71Yrvh3vzvtX4XH6D9MNtX6LZaPZL7fAEtcedM0L0d6rVjXcB53MPM67QUPrUpJiLzntmVjyzT39GBHaKX3klQeWl_GONA0mr2ZVAlylKJQgy6bwSQydf',
    );
    videoId = TextEditingController(text: '6220030527001');
  }

  @override
  void dispose() {
    accountId.dispose();
    policyId.dispose();
    videoId.dispose();
    super.dispose();
  }

  void _onTap(BuildContext context) async {
    final FormState _form = Form.of(context);
    final _isValid = _form.validate();
    if (_isValid) {
      await _openPlayer(context);
    }
  }

  Future<void> _openPlayer(BuildContext context) async {
    final manager = await BrightcoveManager.initialize(
      accountId: accountId.text,
      policyId: policyId.text,
      videoId: videoId.text,
      onCloseCallback: (context) async {
        await Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => Form(
              child: MyHomePage(
                title: title,
              ),
            ),
          ),
        );
      },
    );

    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => BrightcovePlayer(
          manager: manager,
        ),
      ),
    );
  }

  String _validate(String value) {
    if (value?.isEmpty ?? true) {
      return 'Field must not me empty';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            TextFormField(
              controller: accountId,
              decoration: InputDecoration(
                labelText: 'Account ID',
              ),
              validator: _validate,
            ),
            TextFormField(
              controller: policyId,
              decoration: InputDecoration(
                labelText: 'Policy ID',
              ),
              validator: _validate,
            ),
            TextFormField(
              controller: videoId,
              decoration: InputDecoration(
                labelText: 'Video ID',
              ),
              validator: _validate,
            ),
            OutlineButton.icon(
              icon: Icon(
                Icons.play_arrow_outlined,
              ),
              label: Text('Play'),
              onPressed: () => _onTap(context),
            ),
          ],
        ),
      ),
    );
  }
}
